package localization;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class App {
    public static void main(String[] args) {
        Locale locale=new Locale("en", "US");
        Locale locale1=Locale.getDefault();
        Locale locale2=new Locale("de", "GR");

        System.out.println(locale1);

//        Locale[] locales=Locale.getAvailableLocales();
//        for (Locale loc:locales){
//            System.out.println(loc);
//        }

        System.out.println(NumberFormat.getCurrencyInstance(locale).format(1000));
        System.out.println(NumberFormat.getCurrencyInstance(locale1).format(1000));
        System.out.println(NumberFormat.getCurrencyInstance(locale2).format(1000));

        System.out.println(DateFormat.getDateInstance(DateFormat.FULL, locale).format(new Date()));
        System.out.println(DateFormat.getDateInstance(DateFormat.FULL, locale1).format(new Date()));
        System.out.println(DateFormat.getDateInstance(DateFormat.FULL, locale2).format(new Date()));

//        ResourceBundle language = new ResourceBundle.getBundle("language");
//        System.out.println(language.getString("somevalue"));
    }
}
