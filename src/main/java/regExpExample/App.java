package regExpExample;

import java.util.regex.*;

public class App {

    public static final String TEXT = "Мне очень нравится Тайланд. Таиланд это то место куда бы я поехал. тайланд - мечты сбываются!";

    public static void main(String[] args) {

        String pattern = "[A-Za-z]+";

        String text = "Now is the time";

        Pattern p = Pattern.compile(pattern);

        Matcher m = p.matcher(text);

        while (m.find()) {

            System.out.print(text

                    .substring(m.start(), m.end()) + "*");

        }

        int counter=0;

        String s="192.198.1.197";
        Pattern pattern1=Pattern.compile(".*?19");
        Matcher matcher=pattern1.matcher(s);

        while (matcher.find()){
            counter++;
            System.out.println("Matcher found '" + s.substring(matcher.start(),matcher.end())+
                    "'. Starting at index "+matcher.start()+
                    " end ending at index "+matcher.end());
        }
        System.out.println("Matcher found: "+counter);

        System.out.println(TEXT.replaceAll("[Тт]а[ий]ланд", "Украина"));

    }
}
